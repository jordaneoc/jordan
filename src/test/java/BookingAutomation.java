import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class BookingAutomation {

    public String leadNumber = "739003";
    public String fixedLead;
    public String assessor = " blair Magon ";
    public String assessorEmail;
//    public String msPasswordString = "Huc52141";
//    public String esmartPassword = "pvhpnjd2glhf321";
    public String ownershipStatus;
    public String agencyNameStr;
    public String contactNameStr;
    public String contactNumberStr;
    public String leadSource;
    public String firstName;
    public String lastName;
    public String emailAddress;
    public String phoneNumber;
    public String postalAddress;
    public String unitNumber;
    public String streetNumber;
    public String streetName;
    public String streetType;
    public String suburb;
    public String branch;
    public String town;
    public String postCode;
    public String notes;
    public String partialLeadNumber;
    public String occupantFirstName;
    public String occupantMobile;
    public String user;
    public String form;
    public String esPassword;
    public String msPassword;


    public void setUserName(String string){
        if (string.equals("JordanO'Connell")) {
            user = "jordan.o@energysmart.co.nz";
            form = "https://forms.office.com/pages/responsepage.aspx?id=XuOwDZWpZEy32GdfTTuWObcui83EonNLpubUEQrqWzlUMFVYMkE4VzIzRlcwRDZDWFBBSldUOElQMy4u&web=1&wdLOR=c4F79543D-7110-4A81-A505-CFFC3CB65AE0";
            esPassword = "pvhpnjd2glhf321";
            msPassword = "Huc52141";
        }
        if (string.equals("ChrisNelson")) {
            user = "chris.n@energysmart.co.nz";
            form = "https://forms.office.com/pages/responsepage.aspx?id=XuOwDZWpZEy32GdfTTuWObcui83EonNLpubUEQrqWzlUM1NPNjYwRlY1NEJGWTBVVzhNNThSTkpJQS4u&web=1&wdLOR=cC33897D3-35FE-414E-8ED6-E882DD46A2FB";
            esPassword = "Energy21";
            msPassword = "Kal46702";
        }
        if (string.equals("JessicaElder")) {
            user = "jessica.e@energysmart.co.nz";
            form = "https://forms.office.com/pages/responsepage.aspx?id=XuOwDZWpZEy32GdfTTuWObcui83EonNLpubUEQrqWzlUQkdLQUpMR1JFQUdQVVdEWlNTRjE3V1owMS4u";
            esPassword = "Energy21";
            msPassword = "Joh94903";
        }
        if (string.equals("DevikaKrishna")) {
            user = "devika.k@energysmart.co.nz";
            form = "https://forms.office.com/Pages/ResponsePage.aspx?id=XuOwDZWpZEy32GdfTTuWObcui83EonNLpubUEQrqWzlUMkIySjRBOE1USEQ1NTEwSjIyNFdFRzkyRi4u";
            esPassword = "Energy21";
            msPassword = "Cob28664";
        }
    }



    public void setLeadSource(String string){ leadSource = string;}
    public void setOwnershipStatus(String string){ ownershipStatus = string;}
    public void setLeadNumber(String string){leadNumber = string;}
    public void setAgencyName(String string){agencyNameStr = string;}
    public void setContactName(String string){contactNameStr = string;}
    public void setContactNumber(String string){contactNumberStr = string;}
    public void setFirstName(String string){firstName = string;}
    public void setLastName(String string){lastName = string;}
    public void setEmailAddress(String string){emailAddress = string;}
    public void setPhoneNumber(String string){phoneNumber = string;}
    public void setPostalAddress(String string){postalAddress = string;}
    public void setUnitNumber(String string){unitNumber = string;}
    public void setStreetNumber(String string){streetNumber = string;}
    public void setStreetName(String string){streetName = string;}
    public void setStreetType(String string){streetType = string;}
    public void setSuburb(String string){suburb = string;}
    public void setBranch(String string){branch = string;}
    public void setTown(String string){town = string;}
    public void setPostCode(String string){postCode = string;}
    public void setNotes(String string){notes = string;}
    public void setFixedLead(String string){fixedLead = string.trim();}
    public void setPartialLeadNumber(String string){partialLeadNumber = string.substring(0,5);}
    public void setOccupantFirstName(String string){occupantFirstName = string;}
    public void setOccupantMobile(String string){occupantMobile = string;}


    public void setOwnershipStatus(WebDriver driver){
        Select ownershipStatusSelect = new Select(driver.findElement(By.id("lead-ownership_status")));
        WebElement ownershipStatusOption = ownershipStatusSelect.getFirstSelectedOption();
        setOwnershipStatus(ownershipStatusOption.getText());
    }
    public void setLeadSource (WebDriver driver){
        Select leadSourceSelect = new Select(driver.findElement(By.id("lead-source_id")));
        WebElement leadSourceOption = leadSourceSelect.getFirstSelectedOption();
        setLeadSource(leadSourceOption.getText());
    }

    public void setStreetType (WebDriver driver){
        Select streetTypeSelect = new Select(driver.findElement(By.id("property-street_type_id")));
        WebElement streetTypeOption = streetTypeSelect.getFirstSelectedOption();
        setStreetType(streetTypeOption.getText());
    }

    public void adjustUnitAndStreetNumber(){
        if (streetNumber.contains("/")){
            setUnitNumber(streetNumber.substring(0,streetNumber.indexOf("/")));
            int streetNumberLength =streetNumber.length();
            setStreetNumber(streetNumber.substring((streetNumber.lastIndexOf('/')+1),streetNumberLength));
        }
    }

    public void setBranch (WebDriver driver){
        Select branchSelect = new Select(driver.findElement(By.id("leadBranchId")));
        WebElement branchOption = branchSelect.getFirstSelectedOption();
        setBranch(branchOption.getText());
    }

    public void setTown (WebDriver driver) {
        Select townSelect = new Select(driver.findElement(By.id("propertyTownId")));
        WebElement townOption = townSelect.getFirstSelectedOption();
        setTown(townOption.getText());
    }
    public void setAssessorEmail(String string){
        if (string.strip().equalsIgnoreCase("Blair Magon")){assessorEmail = "blair.m@energysmart.co.nz"; assessor = "Blair Magon";}
        if (string.strip().equalsIgnoreCase("Joel Hansen")){assessorEmail = "joel.h@energysmart.co.nz"; assessor = "Joel Hansen";};
        if (string.strip().equalsIgnoreCase("Selby Pope")){assessorEmail = "selby.p@energysmart.co.nz"; assessor = "Selby Pope";};
        if (string.strip().equalsIgnoreCase("Stephen Ramakers")){assessorEmail = "stephen.r@energysmart.co.nz"; assessor = "Stephen Ramakers";};

    }

























}
