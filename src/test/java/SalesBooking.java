import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SalesBooking extends BookingAutomation {

    @FindBy(id = "loginform-email")
    private WebElement email;
    @FindBy(id = "loginform-password")
    private WebElement password;
    @FindBy(name = "login-button")
    private WebElement login;
    @FindBy(id = "lead-ownership_status")
    private WebElement WEdropDownOwnershipStatus;
    @FindBy(id = "customer-first_name")
    private WebElement customerFirstName;
    @FindBy(id = "customer-last_name")
    private WebElement customerLastName;
    @FindBy(id = "customer-email")
    private WebElement customerEmail;
    @FindBy(id = "customer-mobile")
    private WebElement customerMobile;
    @FindBy(id = "customer-postal_address")
    private WebElement customer_postalAddress;
    @FindBy(xpath = "/html/body/div[1]/div/div/div/div/ul/li[2]/a")
    private WebElement ownerTab;
    @FindBy(xpath = "/html/body/div[1]/div/div/div/div/ul/li[4]/a")
    private WebElement propertyTab;
    @FindBy(xpath = "/html/body/div[1]/div/div/div/div/ul/li[3]/a")
    private WebElement tenantTab;
    @FindBy(id = "property-unit_number")
    private WebElement propertyUnitNumber;
    @FindBy(id = "property-street_number")
    private WebElement propertyStreetNumber;
    @FindBy(id = "property-street_name")
    private WebElement propertyStreetName;
    @FindBy(id = "property-street_type_id")
    private WebElement propertyStreetType;
    @FindBy(id = "property-suburb")
    private WebElement propertySuburb;
    @FindBy(id = "leadBranchId")
    private WebElement propertyBranch;
    @FindBy(id = "propertyTownId")
    private WebElement propertyTown;
    @FindBy(id = "property-postcode")
    private WebElement propertyPostCode;
    @FindBy(id = "property-notes")
    private WebElement propertyNotes;
    @FindBy(xpath = "//*[@id=\"i0116\"]")
    private WebElement msEmail;
    @FindBy(xpath = "//*[@id=\"idSIButton9\"]")
    private WebElement msNext;
    @FindBy(xpath = "//*[@id=\"i0118\"]")
    private WebElement msPasswordElement;
    @FindBy(xpath = "//*[@id=\"idSIButton9\"]")
    private WebElement msSignIn;
    @FindBy(xpath = "//*[@id=\"idBtn_Back\"]")
    private WebElement msNoToLoginAgain;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formLeadNumber;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formLeadSource;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div/div/div/div[1]/div/div[1]/div[3]/div[2]/div[2]/div[2]/div/div[2]/div/div[3]/div/label/input")
    private WebElement formOwnership;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[5]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formFirstName;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[6]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formLastName;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[7]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formEmail;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[8]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formPhoneNumber;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[9]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formPostalAddress;
    @FindBy(xpath = "/html/body/div[1]/div/div/div/div[1]/div/div[1]/div[3]/div[3]/div[1]/button/div")
    private WebElement formButtonNext;
    @FindBy(id = "occupantdetails-first_name")
    private WebElement elementOccupantFirstName;
    @FindBy(id = "occupantdetails-mobile")
    private WebElement elementOccupantMobile;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formElementOccupantFirstName;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formElementOccupantMobile;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formUnitNumber ;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formStreetNumber ;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[4]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formStreetName;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[5]/div[1]/div[2]/div[1]/div[12]/div[1]/label[1]/div[1]/div[1]/input[1]")
    private WebElement formStreetType;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[6]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formSuburb;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[7]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formTown;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[9]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formPostCode;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[10]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formNotes;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/select[1]")
    private List<WebElement> listOfSources;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[4]/div/div[2]/div/div")
    private List<WebElement> listOfOwnerShipTypes;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[8]/div[1]/div[2]/div[1]/div[1]/div[1]/i[1]")
    private WebElement dropDownArrow;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[8]/div[1]/div[2]/div[1]/div[1]/div[2]/div[*]")
    private List<WebElement> listOfRegions;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @Test
    public void smarterTest()
    {
        System.out.println("Loading ChromeDriver and initiating all WebElements");
        WebDriver driver = new ChromeDriver();
        PageFactory.initElements(driver, this);

        System.out.println("Correcting input data for improper entry of lead number");
        setUserName(System.getProperty("user.name"));
        setFixedLead(leadNumber);
        setPartialLeadNumber(fixedLead);

        System.out.println("Loading eSmart website");
        driver.get("https://esmart.co.nz/lead/lead/update?id="+partialLeadNumber+"#Lead");
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loginform-email")));

        System.out.println("Logging in to eSmart website");
        email.sendKeys(user);
        password.sendKeys(esPassword);
        login.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("lead-ownership_status")));
        PageFactory.initElements(driver, this);

        System.out.println("Collecting and storing all relevant information from the lead tab");
        setOwnershipStatus(driver);
        setLeadSource(driver);

        System.out.println("Collecting and storing all relevant information from the owner tab");
        ownerTab.click();
        setFirstName(customerFirstName.getAttribute("value"));
        setLastName(customerLastName.getAttribute("value"));
        setEmailAddress(customerEmail.getAttribute("value"));
        setPhoneNumber(customerMobile.getAttribute("value"));
        setPostalAddress(customer_postalAddress.getText());

        //upto here

        System.out.println("Collecting and storing all relevant information from the occupant tab");
        tenantTab.click();
        setOccupantFirstName(elementOccupantFirstName.getAttribute("value"));
        setOccupantMobile(elementOccupantMobile.getAttribute("value"));

        System.out.println("Collecting and storing all relevant information from the property tab");
        propertyTab.click();
        setUnitNumber(propertyUnitNumber.getAttribute("value"));
        setStreetNumber(propertyStreetNumber.getAttribute("value"));
        //This ensures that on the booking form the unit number is actually entered and that no slash is entered
        adjustUnitAndStreetNumber();
        setStreetName(propertyStreetName.getAttribute("value"));
        setStreetType(driver);
        setSuburb(propertySuburb.getAttribute("value"));
        setBranch(driver);
        setTown(driver);
        setPostCode(propertyPostCode.getAttribute("value"));
        setNotes(propertyNotes.getAttribute("value"));

        System.out.println("Load up the booking form and login");
        driver.get(form);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"i0116\"]")));
        msEmail.sendKeys(user);
        msNext.click();
        PageFactory.initElements(driver, this);
        wait.until(ExpectedConditions.visibilityOf(msPasswordElement));
        msPasswordElement.sendKeys(msPassword);
        driver.findElement(By.id("idSIButton9")).click();
        msNoToLoginAgain.click();
        wait.until(ExpectedConditions.visibilityOf(formLeadNumber));


        formLeadNumber.sendKeys(fixedLead);
        for (WebElement element : listOfOwnerShipTypes) {
            if (element.getText().equals(ownershipStatus)) {
                element.click();
            }
        }
        formLeadSource.sendKeys(leadSource);
        formFirstName.sendKeys(firstName);
        formLastName.sendKeys(lastName);
        formEmail.sendKeys(emailAddress);
        formPhoneNumber.sendKeys(phoneNumber);
        formPostalAddress.sendKeys(postalAddress);
        formButtonNext.click();
        formElementOccupantFirstName.sendKeys(occupantFirstName);
        formElementOccupantMobile.sendKeys(occupantMobile);
        driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div[1]/div[2]/div[3]/div[1]/button[2]/div")).click();
        formUnitNumber.sendKeys(unitNumber);
        formStreetNumber.sendKeys(streetNumber);
        formStreetName.sendKeys(streetName);
        formStreetType.sendKeys(streetType);
        formSuburb.sendKeys(suburb);
        formTown.sendKeys(town);
        dropDownArrow.click();
        System.out.println("got here7");
        for (WebElement region : listOfRegions) {
            if (region.getText().equals(branch)) {
                region.click();
                System.out.printf("got here8");
                break;

            }
        }
        formPostCode.sendKeys(postCode);
        formNotes.sendKeys(notes);
        driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div[1]/div[2]/div[3]/div[1]/button[2]/div")).click();


    }
}
