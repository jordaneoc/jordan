import org.apache.commons.compress.archivers.dump.InvalidFormatException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;
import java.util.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class PricingChange {



    @Test
    public void smarterTest() throws IOException {


//  Get details from the bookingsToChange excel
        String inputSpreadsheet = "Sergio Oliveira.xlsx";
        String inputTab = "Sheet1";
        String hpTemplateName = "Heat Pump Quote Template V7.xlsx";
        String insTemplateName = "Insulation Quote Template V6.xlsx";

        int startingRow = 0;

        FileInputStream bookingsToChange = new FileInputStream(new File(inputSpreadsheet));
        Workbook bookings = new XSSFWorkbook(bookingsToChange);
        Sheet bookingsSheet = bookings.getSheet(inputTab);

        int linesToCycle = bookingsSheet.getLastRowNum();
        System.out.println("Reading file "+inputSpreadsheet+ " from the "+inputTab+" tab. Found "+ linesToCycle + " lines to convert");
        int currentLine;
        for (currentLine = startingRow; currentLine <= linesToCycle; currentLine++) {
        System.out.println("Working on line number "+ currentLine);

//        Cell referenceCell = bookingsSheet.getRow(1).getCell(0);
        Cell leadCell = bookingsSheet.getRow(currentLine).getCell(6);
        Cell firstNameCell = bookingsSheet.getRow(currentLine).getCell(8);
        Cell lastNameCell = bookingsSheet.getRow(currentLine).getCell(9);
        Cell unitNumberCell = bookingsSheet.getRow(currentLine).getCell(15);
        Cell streetNumberCell = bookingsSheet.getRow(currentLine).getCell(16);
        Cell streetNameCell = bookingsSheet.getRow(currentLine).getCell(17);
        Cell streetTypeCell = bookingsSheet.getRow(currentLine).getCell(18);
        Cell suburbCell = bookingsSheet.getRow(currentLine).getCell(19);
        Cell townCell = bookingsSheet.getRow(currentLine).getCell(20);
        Cell districtCell = bookingsSheet.getRow(currentLine).getCell(21);
        Cell postcodeCell = bookingsSheet.getRow(currentLine).getCell(22);
        Cell emailCell = bookingsSheet.getRow(currentLine).getCell(10);
        Cell phoneCell = bookingsSheet.getRow(currentLine).getCell(11);
        Cell mobileCell = bookingsSheet.getRow(currentLine).getCell(11);
        Cell postalAddressCell = bookingsSheet.getRow(currentLine).getCell(12);
        Cell ownerTypeCell = bookingsSheet.getRow(currentLine).getCell(7);
        Cell assessorCell = bookingsSheet.getRow(currentLine).getCell(26);
        Cell assessmentDateCell = bookingsSheet.getRow(currentLine).getCell(27);
        Cell regionCell = bookingsSheet.getRow(currentLine).getCell(21);
        Cell bookingDateCell = bookingsSheet.getRow(currentLine).getCell(1);
        Cell tenantNameCell = bookingsSheet.getRow(currentLine).getCell(13);
        Cell tenantPhoneCell = bookingsSheet.getRow(currentLine).getCell(14);

//        String reference = referenceCell.getStringCellValue();
        String lead = leadCell.getStringCellValue();
        String firstName = firstNameCell.getStringCellValue();
        String lastName = lastNameCell.getStringCellValue();
        String unitNumber = unitNumberCell.getStringCellValue();
        String streetNumber = streetNumberCell.getStringCellValue();
        String streetName = streetNameCell.getStringCellValue();
        String streetType = streetTypeCell.getStringCellValue();
        String suburb = suburbCell.getStringCellValue();
        String town = townCell.getStringCellValue();
        String district = districtCell.getStringCellValue();
        String postcode = postcodeCell.getStringCellValue();
        String email = emailCell.getStringCellValue();
        String phone = phoneCell.getStringCellValue();
        String mobile = mobileCell.getStringCellValue();
        String postalAddress = postalAddressCell.getStringCellValue();
        String ownerType = ownerTypeCell.getStringCellValue();
        String assessor = assessorCell.getStringCellValue();
        Date assessmentDate = assessmentDateCell.getDateCellValue();
        String region = regionCell.getStringCellValue();
        Date bookingDate = bookingDateCell.getDateCellValue();
        String tenantName = tenantNameCell.getStringCellValue();
        String tenantPhone = tenantPhoneCell.getStringCellValue();

//  Open the HP template
        FileInputStream heatpumpTemplate = new FileInputStream(new File(hpTemplateName));
        Workbook heatpumpWorkbook = new XSSFWorkbook(heatpumpTemplate);
//        Sheet heatpumpWorksheet = heatpumpWorkbook.getSheet("Set Up");
        Sheet heatpumpWorksheet = heatpumpWorkbook.getSheet("Quotation");

//   Create all the cells
//        Cell referenceT = heatpumpWorksheet.getRow(3).getCell(114);
//        Cell leadT = heatpumpWorksheet.getRow(3).getCell(115);
//        Cell firstNameT = heatpumpWorksheet.getRow(3).getCell(116);
//        Cell lastNameT = heatpumpWorksheet.getRow(3).getCell(117);
//        Cell unitNumberT = heatpumpWorksheet.getRow(3).getCell(118);
//        Cell streetNumberT = heatpumpWorksheet.getRow(3).getCell(119);
//        Cell streetNameT = heatpumpWorksheet.getRow(3).getCell(120);
//        Cell streetTypeT = heatpumpWorksheet.getRow(3).getCell(121);
//        Cell suburbT = heatpumpWorksheet.getRow(3).getCell(122);
//        Cell townT = heatpumpWorksheet.getRow(3).getCell(123);
//        Cell districtT = heatpumpWorksheet.getRow(3).getCell(124);
//        Cell postcodeT = heatpumpWorksheet.getRow(3).getCell(125);
//        Cell emailT = heatpumpWorksheet.getRow(3).getCell(126);
//        Cell phoneT = heatpumpWorksheet.getRow(3).getCell(127);
//        Cell mobileT = heatpumpWorksheet.getRow(3).getCell(128);
//        Cell postalAddressT = heatpumpWorksheet.getRow(3).getCell(129);
//        Cell ownerTypeT = heatpumpWorksheet.getRow(3).getCell(130);
//        Cell assessorT = heatpumpWorksheet.getRow(3).getCell(131);
//        Cell assessmentDateT = heatpumpWorksheet.getRow(3).getCell(132);
//        Cell regionT = heatpumpWorksheet.getRow(3).getCell(133);
//        Cell bookingDateT = heatpumpWorksheet.getRow(3).getCell(134);
//        Cell tenantNameT = heatpumpWorksheet.getRow(3).getCell(135);
//        Cell tenantPhoneT = heatpumpWorksheet.getRow(3).getCell(136);

            Cell leadT = heatpumpWorksheet.getRow(3).getCell(8);
            Cell nameT = heatpumpWorksheet.getRow(11).getCell(2);
            Cell addressT = heatpumpWorksheet.getRow(9).getCell(2);
            Cell assessmentDateT = heatpumpWorksheet.getRow(3).getCell(2);
            Cell customerTypeT = heatpumpWorksheet.getRow(3).getCell(5);
            Cell labelCustomerType = heatpumpWorksheet.getRow(11).getCell(1);
            Cell branchT= heatpumpWorksheet.getRow(5).getCell(8);
            Cell assessorT = heatpumpWorksheet.getRow(7).getCell(8);
            Cell postalAddressT = heatpumpWorksheet.getRow(9).getCell(7);
            Cell emailT = heatpumpWorksheet.getRow(11).getCell(8);
            Cell phoneT = heatpumpWorksheet.getRow(11).getCell(5);

            Sheet heatpumpWorksheetPropertyInputs = heatpumpWorkbook.getSheet("Property Inputs");

            Cell customerTypeTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(1).getCell(1);
            Cell leadTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(3).getCell(1);
            Cell unitNumberTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(10).getCell(1);
            Cell streetNumberTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(11).getCell(1);
            Cell streetNameTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(12).getCell(1);
            Cell streetTypeTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(13).getCell(1);
            Cell suburbTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(14).getCell(1);
            Cell townTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(15).getCell(1);
            Cell districtTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(16).getCell(1);
            Cell postcodeTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(17).getCell(1);
            Cell firstNameTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(20).getCell(1);
            Cell lastNameTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(21).getCell(1);
            Cell emailTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(22).getCell(1);
            Cell telephoneTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(23).getCell(1);
            Cell mobileTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(24).getCell(1);
            Cell postalAddressTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(25).getCell(1);
            Cell tenantNameTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(27).getCell(1);
            Cell tenantPhoneTPropertyInputs = heatpumpWorksheetPropertyInputs.getRow(28).getCell(1);

            customerTypeTPropertyInputs.setCellValue(ownerType);
            leadTPropertyInputs.setCellValue(lead);
            unitNumberTPropertyInputs.setCellValue(unitNumber);
            streetNumberTPropertyInputs.setCellValue(streetNumber);
            streetNameTPropertyInputs.setCellValue(streetName);
            streetTypeTPropertyInputs.setCellValue(streetType);
            suburbTPropertyInputs.setCellValue(suburb);
            townTPropertyInputs.setCellValue(town);
            districtTPropertyInputs.setCellValue(district);
            postcodeTPropertyInputs.setCellValue(postcode);
            firstNameTPropertyInputs.setCellValue(firstName);
            lastNameTPropertyInputs.setCellValue(lastName);
            emailTPropertyInputs.setCellValue(email);
            telephoneTPropertyInputs.setCellValue(mobile);
            mobileTPropertyInputs.setCellValue(mobile);
            postalAddressTPropertyInputs.setCellValue(postalAddress);
            tenantNameTPropertyInputs.setCellValue(tenantName);
            tenantPhoneTPropertyInputs.setCellValue(tenantPhone);

//  Set all cells in HP template with info from the excel
//        leadT.setCellValue(lead);
//        firstNameT.setCellValue(firstName);
//        lastNameT.setCellValue(lastName);
//        unitNumberT.setCellValue(unitNumber);
//        streetNumberT.setCellValue(streetNumber);
//        streetNameT.setCellValue(streetName);
//        streetTypeT.setCellValue(streetType);
//        suburbT.setCellValue(suburb);
//        townT.setCellValue(town);
//        districtT.setCellValue(district);
//        postcodeT.setCellValue(postcode);
//        emailT.setCellValue(email);
//        phoneT.setCellValue(phone);
//        mobileT.setCellValue(mobile);
//        postalAddressT.setCellValue(postalAddress);
//        ownerTypeT.setCellValue(ownerType);
//        assessorT.setCellValue(assessor);
//        assessmentDateT.setCellValue(assessmentDate);
//        regionT.setCellValue(region);
//        bookingDateT.setCellValue(bookingDate);
//        tenantNameT.setCellValue(tenantName);
//        tenantPhoneT.setCellValue(tenantPhone);

            leadT.setCellValue("E"+lead+"A");
            nameT.setCellValue(firstName+" "+lastName);
            addressT.setCellValue(unitNumber+" "+streetNumber+" "+ streetName+" "+ streetType+" "+suburb+" "+town+" "+postcode);
            assessmentDateT.setCellValue(assessmentDate);
            customerTypeT.setCellValue(ownerType);
            labelCustomerType.setCellValue(ownerType);
            branchT.setCellValue(region);
            assessorT.setCellValue(assessor);
            postalAddressT.setCellValue(postalAddress);
            emailT.setCellValue(email);
            phoneT.setCellValue(phone);

//  Save and close HP output
        File folder = new File("C:/Users/EnergySmart/Jordan/selenium-java/PricingChangeOutputs/"+assessor+"/"+lead);
        folder.mkdir();

        File currDirHP = new File("C:/Users/EnergySmart/Jordan/selenium-java/PricingChangeOutputs/"+assessor+"/"+lead);
        String pathHP = currDirHP.getAbsolutePath();
        String fileLocationHP = pathHP+"/"+"HP "+lead+" - "+streetNumber+" "+streetName+" "+streetType+","+suburb+".xlsx";

        FileOutputStream outputStreamHP = new FileOutputStream(fileLocationHP);
        heatpumpWorkbook.write(outputStreamHP);
        heatpumpWorkbook.close();

//  Open insulation template
        FileInputStream insulationTemplate = new FileInputStream(new File(insTemplateName));
        Workbook insulationWorkbook = new XSSFWorkbook(insulationTemplate);
        Sheet insulationWorksheet = insulationWorkbook.getSheet("Quotation");

                                                                                    //        Cell referenceTi = insulationWorksheet.getRow(1).getCell(1);
        Cell leadTi = insulationWorksheet.getRow(3).getCell(8);
        Cell nameTi = insulationWorksheet.getRow(11).getCell(2);
        Cell addressTi = insulationWorksheet.getRow(9).getCell(2);
        Cell assessmentDateTi = insulationWorksheet.getRow(3).getCell(2);
        Cell customerTypeTi = insulationWorksheet.getRow(3).getCell(5);
        Cell labelCustomerTypeTi = insulationWorksheet.getRow(11).getCell(1);
        Cell branchTi = insulationWorksheet.getRow(5).getCell(8);
        Cell assessorTi = insulationWorksheet.getRow(7).getCell(8);
        Cell postalAddressTi = insulationWorksheet.getRow(9).getCell(7);
        Cell emailTi = insulationWorksheet.getRow(11).getCell(8);
        Cell phoneTi = insulationWorksheet.getRow(11).getCell(5);

            Sheet insulationWorksheetPropertyInputs = insulationWorkbook.getSheet("Property Inputs");

            Cell customerTypeTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(1).getCell(1);
            Cell leadTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(3).getCell(1);
            Cell unitNumberTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(9).getCell(1);
            Cell streetNumberTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(10).getCell(1);
            Cell streetNameTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(11).getCell(1);
            Cell streetTypeTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(12).getCell(1);
            Cell suburbTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(13).getCell(1);
            Cell townTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(14).getCell(1);
            Cell districtTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(15).getCell(1);
            Cell postcodeTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(16).getCell(1);
            Cell firstNameTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(19).getCell(1);
            Cell lastNameTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(20).getCell(1);
            Cell emailTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(21).getCell(1);
            Cell telephoneTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(22).getCell(1);
            Cell mobileTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(23).getCell(1);
            Cell postalAddressTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(24).getCell(1);
            Cell tenantNameTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(26).getCell(1);
            Cell tenantPhoneTiPropertyInputs = insulationWorksheetPropertyInputs.getRow(27).getCell(1);

             customerTypeTiPropertyInputs.setCellValue(ownerType);
             leadTiPropertyInputs.setCellValue(lead);
             unitNumberTiPropertyInputs.setCellValue(unitNumber);
             streetNumberTiPropertyInputs.setCellValue(streetNumber);
             streetNameTiPropertyInputs.setCellValue(streetName);
             streetTypeTiPropertyInputs.setCellValue(streetType);
             suburbTiPropertyInputs.setCellValue(suburb);
             townTiPropertyInputs.setCellValue(town);
             districtTiPropertyInputs.setCellValue(district);
             postcodeTiPropertyInputs.setCellValue(postcode);
             firstNameTiPropertyInputs.setCellValue(firstName);
             lastNameTiPropertyInputs.setCellValue(lastName);
             emailTiPropertyInputs.setCellValue(email);
             telephoneTiPropertyInputs.setCellValue(mobile);
             mobileTiPropertyInputs.setCellValue(mobile);
             postalAddressTiPropertyInputs.setCellValue(postalAddress);
             tenantNameTiPropertyInputs.setCellValue(tenantName);
             tenantPhoneTiPropertyInputs.setCellValue(tenantPhone);


//  Set all cells in INS template with info from the excel
                                                                                    //        referenceTi.setCellValue(reference);
        leadTi.setCellValue("I"+lead+"A");
        nameTi.setCellValue(firstName+" "+lastName);
        addressTi.setCellValue(unitNumber+" "+streetNumber+" "+ streetName+" "+ streetType+" "+suburb+" "+town+" "+postcode);
        assessmentDateTi.setCellValue(assessmentDate);
        customerTypeTi.setCellValue(ownerType);
        labelCustomerType.setCellValue(ownerType);
        branchTi.setCellValue(region);
        assessorTi.setCellValue(assessor);
        postalAddressTi.setCellValue(postalAddress);
        emailTi.setCellValue(email);
        phoneTi.setCellValue(phone);


        File currDirINS = new File("C:/Users/EnergySmart/Jordan/selenium-java/PricingChangeOutputs/"+assessor+"/"+lead);
        String pathINS = currDirINS.getAbsolutePath();

        String fileLocationINS = pathINS+"/"+"INS "+lead+" - "+streetNumber+" "+streetName+" "+streetType+","+suburb+".xlsx";

        FileOutputStream outputStreamINS = new FileOutputStream(fileLocationINS);
        insulationWorkbook.write(outputStreamINS);
        insulationWorkbook.close();
        System.out.println("Completed excel conversion for  " + pathINS);
        }

    }
}
