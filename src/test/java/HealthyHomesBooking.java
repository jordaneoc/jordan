import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

public class HealthyHomesBooking extends BookingAutomation {

    @FindBy(id = "loginform-email")
    private WebElement email;
    @FindBy(id = "loginform-password")
    private WebElement password;
    @FindBy(name = "login-button")
    private WebElement login;
    @FindBy(id = "lead-ownership_status")
    private WebElement WEdropDownOwnershipStatus;

    @FindBy(id = "property-agency_contact_name")
    private WebElement esContactName;
    @FindBy(id = "property-agency_contact_phone")
    private WebElement esContactNumber;

    @FindBy(id = "customer-first_name")
    private WebElement customerFirstName;
    @FindBy(id = "customer-last_name")
    private WebElement customerLastName;
    @FindBy(id = "customer-email")
    private WebElement customerEmail;
    @FindBy(id = "customer-mobile")
    private WebElement customerMobile;
    @FindBy(id = "customer-postal_address")
    private WebElement customer_postalAddress;
    @FindBy(xpath = "/html/body/div[1]/div/div/div/div/ul/li[2]/a")
    private WebElement ownerTab;
    @FindBy(xpath = "/html/body/div[1]/div/div/div/div/ul/li[4]/a")
    private WebElement propertyTab;
    @FindBy(xpath = "/html/body/div[1]/div/div/div/div/ul/li[3]/a")
    private WebElement tenantTab;
    @FindBy(id = "property-unit_number")
    private WebElement propertyUnitNumber;
    @FindBy(id = "property-street_number")
    private WebElement propertyStreetNumber;
    @FindBy(id = "property-street_name")
    private WebElement propertyStreetName;
    @FindBy(id = "property-street_type_id")
    private WebElement propertyStreetType;
    @FindBy(id = "property-suburb")
    private WebElement propertySuburb;
    @FindBy(id = "leadBranchId")
    private WebElement propertyBranch;
    @FindBy(id = "propertyTownId")
    private WebElement propertyTown;
    @FindBy(id = "property-postcode")
    private WebElement propertyPostCode;
    @FindBy(id = "property-notes")
    private WebElement propertyNotes;
    @FindBy(xpath = "//*[@id=\"i0116\"]")
    private WebElement msEmail;
    @FindBy(xpath = "//*[@id=\"idSIButton9\"]")
    private WebElement msNext;
    @FindBy(xpath = "//*[@id=\"i0118\"]")
    private WebElement msPassword;
    @FindBy(xpath = "//*[@id=\"idSIButton9\"]")
    private WebElement msSignIn;
    @FindBy(xpath = "//*[@id=\"idBtn_Back\"]")
    private WebElement msNoToLoginAgain;
    @FindBy(xpath = "//div[contains(@class,'office-form-question-body')]//div[2]//div[1]//div[2]//div[1]//div[1]//input[1]")
    private WebElement formLeadNumber;
    @FindBy(xpath = "/html/body/div[1]/div/div/div/div[1]/div/div[1]/div[3]/div[2]/div[2]/div[2]/div/div[2]/div/div[3]/div/label/input")
    private WebElement formOwnership;

    @FindBy(xpath = "//div[4]//div[1]//div[2]//div[1]//div[1]//input[1]")
    private WebElement formAgencyName;
    @FindBy(xpath = "//div[5]//div[1]//div[2]//div[1]//div[1]//input[1]")
    private WebElement formContactName;
    @FindBy(xpath = "//div[6]//div[1]//div[2]//div[1]//div[1]//input[1]")
    private WebElement formContactPhone;
    @FindBy(xpath = "//div[7]//div[1]//div[2]//div[1]//div[1]//input[1]")
    private WebElement formCustomerFirstName;
    @FindBy(xpath = "//div[8]//div[1]//div[2]//div[1]//div[1]//input[1]")
    private WebElement formCustomerLastName;
    @FindBy(xpath = "//div[9]//div[1]//div[2]//div[1]//div[1]//input[1]")
    private WebElement formCustomerEmail;
    @FindBy(xpath = "//div[10]//div[1]//div[2]//div[1]//div[1]//input[1]")
    private WebElement formCustomerPhone;
    @FindBy(xpath = "//div[11]//div[1]//div[2]//div[1]//div[1]//input[1]")
    private WebElement formPostalAddress;

    @FindBy(xpath = "/html/body/div[1]/div/div/div/div[1]/div/div[1]/div[3]/div[3]/div[1]/button/div")
    private WebElement formButtonNext;
    @FindBy(id = "occupantdetails-first_name")
    private WebElement elementOccupantFirstName;
    @FindBy(id = "occupantdetails-mobile")
    private WebElement elementOccupantMobile;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formElementOccupantFirstName;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formElementOccupantMobile;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formUnitNumber ;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formStreetNumber ;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[4]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formStreetName;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[5]/div[1]/div[2]/div[1]/div[12]/div[1]/label[1]/div[1]/div[1]/input[1]")
    private WebElement formStreetType;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[6]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formSuburb;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[7]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formTown;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[9]/div[1]/div[2]/div[1]/div[1]/input[1]")
    private WebElement formPostCode;
    @FindBy(xpath = "/html/body/div[1]/div/div/div/div[1]/div/div[1]/div[2]/div[2]/div[2]/div[9]/div/div[2]/div/div/input")
    private WebElement formNotes;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[3]/div[1]/select[1]")
    private List<WebElement> listOfSources;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]/div[3]/div[1]/div[2]/div[1]/div")
    private List<WebElement> listOfOwnerShipTypes;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[8]/div[1]/div[2]/div[1]/div[1]/div[1]/i[1]")
    private WebElement dropDownArrow;
    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/div[8]/div[1]/div[2]/div[1]/div[1]/div[2]/div")
    private List<WebElement> listOfRegions;

    private final String user = "danielle.h@energysmart.co.nz";
    private final String notusPassword = "Will9600";
    private final String msPasswordStr = "Will770170";
    private final String bookingformLink = "https://forms.office.com/r/8a4Qe6S7T3";
    private String leadNumber = "111111";
    private String assessor = " blair Magon ";
    private String customerFirstNameStr;
    private String customerLastNameStr;
    private String customerEmailStr;
    private String customerPhoneStr;
    private String postalAddressStr;

    //This method will run once before all of the tests in our class
    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    public void setFirstName(String string){customerFirstNameStr = string;}
    public void setLastName(String string){customerLastNameStr = string;}
    public void setEmailAddress(String string){customerEmailStr = string;}
    public void setPhoneNumber(String string){customerPhoneStr = string;}
    public void setPostalAddress(String string){postalAddressStr = string;}

    @Test
    public void smarterTest()
    {
        WebDriver driver = new ChromeDriver();

        PageFactory.initElements(driver, this);
        setLeadNumber(leadNumber);
        setPartialLeadNumber(leadNumber);
        setAssessorEmail(assessor);

        driver.get("https://esmart.co.nz/lead/lead/update?id="+partialLeadNumber+"#Lead");

        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loginform-email")));
        email.sendKeys(user);
        password.sendKeys(notusPassword);
        login.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("lead-ownership_status")));
        PageFactory.initElements(driver, this);

        Select ownershipStatusSelect = new Select(driver.findElement(By.id("lead-ownership_status")));
        WebElement ownershipStatusOption = ownershipStatusSelect.getFirstSelectedOption();
        setOwnershipStatus(ownershipStatusOption.getText());
        if (ownershipStatus.equals("Agency")){
            Select agencySelect = new Select(driver.findElement(By.id("property-agency_id")));
            WebElement agencyOption = agencySelect.getFirstSelectedOption();
            setAgencyName(agencyOption.getText());
            setContactName(esContactName.getAttribute("value"));
            setContactNumber(esContactNumber.getAttribute("value"));
        }


        ownerTab.click();
        //wait.until(ExpectedConditions.visibil
        // ityOfElementLocated(By.id("customer-first_name")));

        setFirstName(customerFirstName.getAttribute("value"));
        setLastName(customerLastName.getAttribute("value"));
        setEmailAddress(customerEmail.getAttribute("value"));
        setPhoneNumber(customerMobile.getAttribute("value"));
        setPostalAddress(customer_postalAddress.getText());

        tenantTab.click();

        setOccupantFirstName(elementOccupantFirstName.getAttribute("value"));
        setOccupantMobile(elementOccupantMobile.getAttribute("value"));

        propertyTab.click();

        setUnitNumber(propertyUnitNumber.getAttribute("value"));
        setStreetNumber(propertyStreetNumber.getAttribute("value"));
        if (propertyStreetNumber.getAttribute("value").contains("/")){

            setUnitNumber(streetNumber.substring(0,streetNumber.indexOf("/")));
            int streetNumberLength =streetNumber.length();
            setStreetNumber(streetNumber.substring((streetNumber.lastIndexOf('/')+1),streetNumberLength));

        }

        setStreetName(propertyStreetName.getAttribute("value"));
        Select streetTypeSelect = new Select(driver.findElement(By.id("property-street_type_id")));
        WebElement streetTypeOption = streetTypeSelect.getFirstSelectedOption();
        setStreetType(streetTypeOption.getText());

        setSuburb(propertySuburb.getAttribute("value"));

        Select branchSelect = new Select(driver.findElement(By.id("leadBranchId")));
        WebElement branchOption = branchSelect.getFirstSelectedOption();
        setBranch(branchOption.getText());

        Select townSelect = new Select(driver.findElement(By.id("propertyTownId")));
        WebElement townOption = townSelect.getFirstSelectedOption();
        setTown(townOption.getText());


        setPostCode(propertyPostCode.getAttribute("value"));
        setNotes(propertyNotes.getAttribute("value"));

        driver.get(bookingformLink);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"i0116\"]")));

        msEmail.sendKeys(user);
        msNext.click();
        PageFactory.initElements(driver, this);
        wait.until(ExpectedConditions.visibilityOf(msPassword));
        msPassword.sendKeys(msPasswordStr);
        driver.findElement(By.id("idSIButton9")).click();

        msNoToLoginAgain.click();
        wait.until(ExpectedConditions.visibilityOf(formLeadNumber));
        formLeadNumber.sendKeys(leadNumber);
        for (WebElement element : listOfOwnerShipTypes) {
            if (element.getText().equals(ownershipStatus)) {
                element.click();
            }
        }
        formAgencyName.sendKeys(agencyNameStr);
        formContactName.sendKeys(contactNameStr);
        formContactPhone.sendKeys(contactNumberStr);
        formCustomerFirstName.sendKeys(customerFirstNameStr);
        formCustomerLastName.sendKeys(customerLastNameStr);
        formCustomerEmail.sendKeys(customerEmailStr);
        formCustomerPhone.sendKeys(customerPhoneStr);
        formPostalAddress.sendKeys(postalAddressStr);
        formButtonNext.click();
        formElementOccupantFirstName.sendKeys(occupantFirstName);
        formElementOccupantMobile.sendKeys(occupantMobile);
        driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div[1]/div[2]/div[3]/div[1]/button[2]/div")).click();
        formUnitNumber.sendKeys(unitNumber);
        formStreetNumber.sendKeys(streetNumber);
        formStreetName.sendKeys(streetName);
        formStreetType.sendKeys(streetType);
        formSuburb.sendKeys(suburb);
        formTown.sendKeys(town);
        dropDownArrow.click();
        System.out.println("got here7");
        for (WebElement region : listOfRegions) {
            if (region.getText().equals(branch)) {
                region.click();
                System.out.printf("got here8");
                break;

            }
        }
        formPostCode.sendKeys(postCode);
//        formNotes.sendKeys(notes);
        driver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[1]/div/div[1]/div[2]/div[3]/div[1]/button[2]/div")).click();
        driver.findElement(By.xpath("//input[@type='radio'][@value='"+assessorEmail+"']")).click();




    }
}
